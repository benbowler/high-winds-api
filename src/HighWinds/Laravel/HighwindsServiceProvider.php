<?php
/**
 * 
 */

namespace Highwinds\Laravel;

// use Highwinds\Common\Accounts;
// use Highwinds\Common\Http;
use Highwinds\Laravel\Highwinds;
// use Guzzle\Common\Event;
// use Guzzle\Service\Client;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;

/**
 * highwinds SDK for PHP service provider for Laravel applications
 */
class HighwindsServiceProvider extends ServiceProvider
{
    const VERSION = '1.0.1';


    // Do the following functions do anything? These are copied from AWS

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('highwinds', function()
        {
            return new Highwinds;
        });
    }

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        // $this->package('highwinds/highwinds-sdk-php-laravel', 'highwinds');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array('highwinds');
    }





    public function getRequest($endpoint, $method = 'GET') {
        $options = [];
        // $url = 'http://localhost/rest/index.php';
        $url = $this->baseUrl;

        $headers = array(
            "Content-Type: text/xml",
            "Accept: text/xml",
        );

        $get = ['user' => 'default'];
        $defaults = array(
            CURLOPT_URL => $url . (strpos($url, '?') === FALSE ? '?' : '') . http_build_query($get),
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_HEADER => 1,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_TIMEOUT => 4,
            CURLOPT_CUSTOMREQUEST => 'GET',
            //CURLOPT_POST => true,
            //CURLOPT_PUT => '',
            // CURLOPT_POSTFIELDS => "$xml",
            CURLOPT_USERPWD => Config::get('highwinds.username').':'.Config::get('highwinds.password')
        );
        $ch = curl_init();

        curl_setopt_array($ch, ($options + $defaults));

        $result = curl_exec($ch); //) {
        //  trigger_error(curl_error($ch));
        // }
        //@curl_setopt($ch, CURLOPT_VERBOSE, 1);

        curl_close($ch);
        return $result;

    }
}