<?php
/**
 * 
 */

namespace Highwinds\Laravel;

// use Aws\Common\Client\AwsClientInterface;
use Illuminate\Support\Facades\Facade;

/**
 * Facade for the AWS service
 *
 * @method static AwsClientInterface get($name, $throwAway = false) Get a client from the service builder
 */
class HighwindsFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'highwinds';
    }

    // private $baseUrl = 'https://striketracker2.highwinds.com/';

    // public function getRequest($username, $password, $endpoint, $method = 'GET') {
    //     $options = [];
    //     // $url = 'http://localhost/rest/index.php';
    //     $url = $this->baseUrl;

    //     $headers = array(
    //         "Content-Type: text/xml",
    //         "Accept: text/xml",
    //     );

    //     $get = ['user' => 'default'];
    //     $defaults = array(
    //         CURLOPT_URL => $url . (strpos($url, '?') === FALSE ? '?' : '') . http_build_query($get),
    //         CURLOPT_HTTPHEADER => $headers,
    //         CURLOPT_HEADER => 1,
    //         CURLOPT_RETURNTRANSFER => TRUE,
    //         CURLOPT_TIMEOUT => 4,
    //         CURLOPT_CUSTOMREQUEST => 'GET',
    //         //CURLOPT_POST => true,
    //         //CURLOPT_PUT => '',
    //         // CURLOPT_POSTFIELDS => "$xml",
    //         CURLOPT_USERPWD => $username.':'.$password
    //     );
    //     $ch = curl_init();

    //     curl_setopt_array($ch, ($options + $defaults));

    //     $result = curl_exec($ch); //) {
    //     //  trigger_error(curl_error($ch));
    //     // }
    //     //@curl_setopt($ch, CURLOPT_VERBOSE, 1);

    //     curl_close($ch);
    //     return $result;

    // }

}
