<?php
/**
 * 
 */

namespace Highwinds\Laravel;

// use Illuminate\Config\Repository;
// use Illuminate\Config\Repository\Config;

/**
 * highwinds SDK for PHP service provider for Laravel applications
 */
class Highwinds {

    private $baseUrl = 'https://striketracker2.highwinds.com';

    public function purge($username, $password, $recursive, $url) {

        $this->getRequest(
                Config::get('highwinds.username'),
                Config::get('highwinds.password'),
                '/webservices/content/',
                array('recursive' => $recursive, 'url' => $url),
                'DELETE');
    }

    public function getRequest($username, $password, $endpoint, $options, $method = 'GET') {

        $url = $this->baseUrl;

        $headers = array(
            "Content-Type: application/xml",
            "Accept: application/xml",
        );

// dd(http_build_query($options));

        $curl_options = array(
            CURLOPT_URL => $url . $endpoint . '?' . urldecode(http_build_query($options)),
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_HEADER => 1,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_TIMEOUT => 4,
            CURLOPT_CUSTOMREQUEST => $method,
            // CURLOPT_POST => true,
            // CURLOPT_PUT => '',
            // CURLOPT_POSTFIELDS => "$xml",
            CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
            CURLOPT_USERPWD => $username.':'.$password
        );

        // dd($curl_options);

        $ch = curl_init();

        curl_setopt_array($ch, $curl_options);

        $result = curl_exec($ch); //) {
        //  trigger_error(curl_error($ch));
        // }
        //@curl_setopt($ch, CURLOPT_VERBOSE, 1);

        curl_close($ch);
        return $result;

    }
}